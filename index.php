<?php
/**
 * The main template file
 *
 * This is the most generic template file in a WordPress theme
 * and one of the two required files for a theme (the other being style.css).
 * It is used to display a page when nothing more specific matches a query.
 * e.g., it puts together the home page when no home.php file exists.
 *
 * Learn more: {@link https://codex.wordpress.org/Template_Hierarchy}
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<div class="bb-custom-wrapper" >
<div id="bb-bookblock" class="bb-bookblock">
<?php
	query_posts(array(
		'post_type' => 'page',
		'order' => 'ASC',
		'orderby' => 'menu_order',
		'posts_per_page' => 25

		));

		$counter = 1;

		while (have_posts()) { the_post(); ?>




<div id="slide-<?php echo $counter; ?>" class="bb-item slide-<?php echo $counter; ?>" style="background-image: url(<?php
	the_field('background_image');
	?>); background-repeat: no-repeat;background-position: 50% 50%; background-size: cover;">
			<div class="content" style="position: relative; z-index: 100;">
				<div class="scroller">

					<?php if ( ! get_field( 'hide_title' ) ): ?>

						<h1><?php the_title(); ?></h1>

					<?php else: // field_name returned false ?>



					<?php endif; // end of if field_name logic ?>

					<?php get_template_part( 'parts/columns' ); ?>
				</div>
			</div>

			<div class="drum">
				<a href="https://drumcreative.com" target="_blank">
				<?php $background = get_field('background_image'); ?>
				<?php if ($background == get_site_url() .'/wp-content/uploads/2016/04/1920_1080__red_bg.png') : ?>
					<!-- reverse logo -->
					<img width="125" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/white_logo@2x.png"/>
				<?php elseif ($background == get_site_url() .'/wp-content/uploads/2016/05/doc.gif') :  ?>
					<!-- reverse logo -->
					<img width="125" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/white_red_logo@2x.png"/>
				<?php elseif ($background == get_site_url() .'/wp-content/uploads/2016/04/demo-time.jpg') :  ?>
					<!-- reverse logo -->
					<img width="125" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/white_red_logo@2x.png"/>
				<?php else : ?>
					<!-- normal logo -->
					<img width="125" src="<?php echo get_stylesheet_directory_uri(); ?>/assets/images/logo@2x.png"/>
				<?php endif; ?>
				</a>
			</div>

		</div>



<?php $counter++; ?>

	<?php } ?>

	</div>

<nav>
	<span id="bb-nav-prev">&larr;</span>
	<span id="bb-nav-next">&rarr;</span>
</nav>



</div>
<?php get_footer(); ?>
<script>
	jQuery( document ).ready(function() {

		Page.init();

	});
</script>