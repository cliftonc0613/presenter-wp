<?php
/**
 * Author: Clifton Canady
 * URL: http://cliftoncanady.co/
 *
 * FoundationPress functions and definitions
 *
 * Set up the theme and provides some helper functions, which are used in the
 * theme as custom template tags. Others are attached to action and filter
 * hooks in WordPress to change core functionality.
 *
 * @link https://codex.wordpress.org/Theme_Development
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

/** Various clean up functions */
require_once( 'library/cleanup.php' );

/** Required for Foundation to work properly */
require_once( 'library/foundation.php' );

/** Register all navigation menus */
require_once( 'library/navigation.php' );

/** Add menu walkers for top-bar and off-canvas */
//require_once( 'library/menu-walkers.php' );

/** Create widget areas in sidebar and footer */
//require_once( 'library/widget-areas.php' );

/** Return entry meta information for posts */
require_once( 'library/entry-meta.php' );

/** Enqueue scripts */
require_once( 'library/enqueue-scripts.php' );

/** Add theme support */
require_once( 'library/theme-support.php' );

/** Add Nav Options to Customer */
//require_once( 'library/custom-nav.php' );

/** Change WP's sticky post class */
//require_once( 'library/sticky-posts.php' );

/** Change WP's sticky post class */
//require_once( 'library/ctwd-shortcodes.php' );

/** If your site requires protocol relative url's for theme assets, uncomment the line below */
// require_once( 'library/protocol-relative-theme-assets.php' );


add_filter('acf/settings/save_json', 'my_acf_json_save_point');

function my_acf_json_save_point( $path ) {

    // update path
    $path = get_stylesheet_directory() . '/acf-json';


    // return
    return $path;

}


// style editor
remove_filter( 'the_content', 'wpautop' );
remove_filter( 'the_content', 'wptexturize' );
remove_filter( 'the_excerpt', 'wpautop' );
remove_filter( 'the_excerpt', 'wptexturize' );
remove_filter( 'acf_the_content', 'wpautop' );
remove_filter( 'acf_the_content', 'wptexturize' );

// markdown support
add_filter( 'the_content', 'Markdown' );
add_filter( 'the_excerpt', 'Markdown' );
add_filter( 'acf_the_content', 'Markdown' );

?>
