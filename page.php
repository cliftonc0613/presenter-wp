<?php
/**
 * The template for displaying pages
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

get_header(); ?>

<?php get_template_part( 'parts/featuredimage' ); ?>


<div class="no-padding" role="main">

<?php /* Start loop */ ?>
<?php while ( have_posts() ) : the_post(); ?>
	<article <?php post_class() ?> id="post-<?php the_ID(); ?>">

		<div class="entry-content" itemprop="mainContentOfPage">
			<?php
				// Include the page content template.
				get_template_part( 'content', 'page' );
			?>
		</div>
	</article>
<?php endwhile; // End the loop ?>

</div>
<?php get_footer(); ?>
