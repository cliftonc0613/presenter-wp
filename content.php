<?php
/**
 * The default template for displaying content
 *
 * Used for both single and index/archive/search.
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

?>

<div id="post-<?php the_ID(); ?>" <?php post_class('blogpost-entry'); ?>>
	<div class="bb-custom-wrapper">
	<div id="bb-bookblock" class="bb-bookblock">
		<div class="bb-item" id="item1">
			<div class="content">
				<div class="scroller">
					<h2><?php the_title(); ?></h2>
					<?php get_template_part( 'parts/columns' ); ?>
				</div>
			</div>
		</div>
	</div>

	<nav>
		<span id="bb-nav-prev">&larr;</span>
		<span id="bb-nav-next">&rarr;</span>
	</nav>
	</div>
</div>
