<?php
/**
 * Enqueue all styles and scripts
 *
 * Learn more about enqueue_script: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_script}
 * Learn more about enqueue_style: {@link https://codex.wordpress.org/Function_Reference/wp_enqueue_style }
 *
 * @package WordPress
 * @subpackage FoundationPress
 * @since FoundationPress 1.0.0
 */

if ( ! function_exists( 'foundationpress_scripts' ) ) :
	function foundationpress_scripts() {

		// Enqueue the main Stylesheet.
		wp_enqueue_style( 'main-stylesheet', get_stylesheet_directory_uri() . '/assets/stylesheets/foundation.css' );

	// Modernizr is used for polyfills and feature detection. Must be placed in header. (Not required).
	wp_enqueue_script( 'modernizr', get_template_directory_uri() . '/assets/javascript/vendor/modernizr.js', array(), '2.8.3', false );

	// Fastclick removes the 300ms delay on click events in mobile environments. Must be placed in header. (Not required).
	wp_enqueue_script( 'fastclick', get_template_directory_uri() . '/assets/javascript/vendor/fastclick.js', array(), '1.0.0', false );

		// If you'd like to cherry-pick the foundation components you need in your project, head over to Gruntfile.js and see lines 124-142.
		// It's a good idea to do this, performance-wise. No need to load everything if you're just going to use the grid anyway, you know :)
		wp_enqueue_script( 'foundation', get_template_directory_uri() . '/assets/javascript/foundation.js', array('jquery'), '2.0.0', true );

		wp_enqueue_script( 'a-mousewheel', get_template_directory_uri() . '/assets/javascript/custom/jquery.a-mousewheel.js', array('jquery'), true );
		wp_enqueue_script( 'b-jscrollpane', get_template_directory_uri() . '/assets/javascript/custom/jquery.b-jscrollpane.min.js', array('jquery'), true );
		wp_enqueue_script( 'c-custom', get_template_directory_uri() . '/assets/javascript/custom/jquery.c-custom.js', array('jquery'), true );
		wp_enqueue_script( 'd-bookblock', get_template_directory_uri() . '/assets/javascript/custom/jquery.d-bookblock.js', array('jquery'), true );
		wp_enqueue_script( 'page-js', get_template_directory_uri() . '/assets/javascript/custom/page.js', array('jquery'), true );


		// Add the comment-reply library on pages where it is necessary
		if ( is_singular() && comments_open() && get_option( 'thread_comments' ) ) {
			wp_enqueue_script( 'comment-reply' );
		}

		// register scripts
		wp_register_script( 'header-scripts', get_template_directory_uri() . '/assets/javascript/header-scripts.js', array(), '1.0.0', false );
		wp_register_script( 'footer-scripts', get_template_directory_uri() . '/assets/javascript/footer-scripts.js', array(), '1.0.0', true );

		// enqueue scripts
		wp_enqueue_script('header-scripts');
		wp_enqueue_script('footer-scripts');
		wp_enqueue_script('a-mousewheel');
		wp_enqueue_script('b-jscrollpane');
		wp_enqueue_script('c-custom');
		wp_enqueue_script('d-bookblock');
		wp_enqueue_script('page-js');


	}

	add_action( 'wp_enqueue_scripts', 'foundationpress_scripts' );
endif;

// Load YoastSEO.js + ACF Fields
function yoast_seo_acf_js($hook) {
    if ( 'edit.php' != $hook && 'post.php' != $hook) {
        return;
    }
    wp_enqueue_script( 'acf_yoastseo', get_template_directory_uri() . '/assets/javascript/acf_yoastseo.js' );
}
add_action( 'admin_enqueue_scripts', 'yoast_seo_acf_js' );
?>
