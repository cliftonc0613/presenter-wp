<?php
// Add button shortcode
add_shortcode( 'button', 'button_shortcode' );
function button_shortcode( $atts, $content = null ) {
    extract( shortcode_atts( array(
        'size' => 'normal',
        'type' => 'primary',
        'align' => 'text-left'
    ), $atts ) );
	$html = '<div class="button ' . $size . ' ' . $type . ' ' . $align . '">' . $content . '</div>';
    return $html;
}

// Shortcode for tooltips
add_shortcode( 'tooltip', 'zurb_tooltip' );
function zurb_tooltip( $atts, $content ){
	extract(shortcode_atts(array(
		'text' => '%d'
	), $atts));
	return '<span data-tooltip class="has-tip tip-bottom" title="' . $text . '">'.$content.'</span>';
}
?>