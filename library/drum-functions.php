<?php
/**
 * Useful function(s) included in this library:
 *
 * 1. has_gform() - Checks to see if a Gravity Forms form is on the current page.
 */	

// Set content width to appease Theme Check
if ( ! isset( $content_width ) ) $content_width = 1024;

// Add Theme Support for the title tag
add_theme_support( "title-tag" );

// Change gallery "Link To" default to be "Media File"
function my_gallery_default_type_set_link( $settings ) {
    $settings['galleryDefaults']['link'] = 'file';
    return $settings;
}
add_filter( 'media_view_settings', 'my_gallery_default_type_set_link');

//Add the read more link to excerpts
function new_excerpt_more( $more ) {
	global $post;
	return '... <a href="'. get_permalink($post->ID) . '">Read More</a>';
}
add_filter('excerpt_more', 'new_excerpt_more');

// Add post/page name to body class
function post_name_in_body_class( $classes ){
	if( is_singular() )
	{
		global $post;
		array_push( $classes, "{$post->post_name}" );
	}
	return $classes;
}
add_filter( 'body_class', 'post_name_in_body_class' );

// Filter to process shortcodes in widgets
add_filter('widget_text', 'do_shortcode');

// Function to strip line breaks (Required to make the Reveal modal window process WYSIWYG content)
function replace_line_breaks($str, $replace = '') {
	$chars = array("
", "\n", "\r", "chr(13)",  "\t", "\0", "\x0B");
	return str_replace($chars, $replace, $str);
}

// Change the media link type to None by default
update_option('image_default_link_type','none');

// Show full WYSIWYG toolbar by default
function unhide_kitchensink( $args ) {
	$args['wordpress_adv_hidden'] = false;
	return $args;
}
add_filter( 'tiny_mce_before_init', 'unhide_kitchensink' );

// show admin bar only for admins and editors
if (!current_user_can('edit_posts')) {
	add_filter('show_admin_bar', '__return_false');
}

// Deny Comment Posting to No Referrer Requests
function check_referrer() {
    if (!isset($_SERVER['HTTP_REFERER']) || $_SERVER['HTTP_REFERER'] == "") {
        wp_die( __('Please enable referrers in your browser, or, if you\'re a spammer, bugger off!', 'drum-beat-6-starter-theme') );
    }
}
add_action('check_comment_flood', 'check_referrer');

//Remove query strings from static resources
function _remove_query_strings_1( $src ){
	$rqs = explode( '?ver', $src );
        return $rqs[0];
}
function _remove_query_strings_2( $src ){
	$rqs = explode( '&ver', $src );
        return $rqs[0];
}
add_filter( 'script_loader_src', '_remove_query_strings_1', 15, 1 );
add_filter( 'style_loader_src', '_remove_query_strings_1', 15, 1 );
add_filter( 'script_loader_src', '_remove_query_strings_2', 15, 1 );
add_filter( 'style_loader_src', '_remove_query_strings_2', 15, 1 );

// conditional to check whether Gravity Forms shortcode is on a page
function has_gform() {
     global $post;
     $all_content = get_the_content();
     if (strpos($all_content,'[gravityform') !== false) {
	return true;
     } else {
	return false;
     }
}

// White Label Stuff
function login_logo() { ?>
<style type="text/css">
        body.login div#login h1 a {
            background-image: url(<?php echo get_template_directory_uri() ?>/assets/images/icons/login-logo-323x67-max.png);
            background-size: auto auto !important;
            width: 100%;
        }

</style>
<?php }
add_action( 'login_enqueue_scripts', 'login_logo' );

function custom_logo() {
	echo '<style type="text/css">
	#wpadminbar > #wp-toolbar > #wp-admin-bar-root-default #wp-admin-bar-wp-logo .ab-icon {
		background: url(' . get_template_directory_uri() . '/assets/images/icons/admin-logo-20x20.png) center center no-repeat !important;
		width: 20px;
		height: 32px;
		padding: 0;
	}
	#wpadminbar > #wp-toolbar > #wp-admin-bar-root-default #wp-admin-bar-wp-logo .ab-icon:before {
		display: none !important;
	}
	</style>';
}
add_action('admin_head', 'custom_logo');

function admin_footer () {
	echo '<a href="http://www.drumcreative.com" target="_blank">&copy;' . date('Y') . ' Drum Creative</a>';
}
add_filter('admin_footer_text', 'admin_footer');

add_filter( 'login_headerurl', 'custom_loginlogo_url' );
function custom_loginlogo_url($url) {
	return 'http://www.drumcreative.com';
}

// Filter Gravity Forms emails through WP Better Emails
add_filter('gform_notification', 'change_notification_format', 10, 3);
function change_notification_format( $notification, $form, $entry ) {
    // is_plugin_active is not availble on front end
    if( !is_admin() )
        include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
    // does WP Better Emails exists and activated ?
    if( !is_plugin_active('wp-better-emails/wpbe.php') )
        return $notification;

    // change notification format to text from the default html
    $notification['message_format'] = "text";
    // disable auto formatting so you don't get double line breaks
    $notification['disableAutoformat'] = true;

    return $notification;
}

// Remove "You may use these HTML tags..." from comments
add_filter( 'comment_form_defaults', 'remove_comment_form_allowed_tags' );
function remove_comment_form_allowed_tags( $defaults ) {

	$defaults['comment_notes_after'] = '';
	return $defaults;
}

// Strip header tag contents from excerpts so excerpts don't read weird
function wp_strip_header_tags( $excerpt='' ) {
	$raw_excerpt = $excerpt;
	if ( '' == $excerpt ) {
		$excerpt = get_the_content('');
		$excerpt = strip_shortcodes( $excerpt );
		$excerpt = apply_filters('the_content', $excerpt);
		$excerpt = str_replace(']]>', ']]&gt;', $excerpt);
	}
	$regex = '#(<h([1-6])[^>]*>)\s?(.*)?\s?(<\/h\2>)#';
	$excerpt = preg_replace($regex,'', $excerpt);
	$excerpt_length = apply_filters('excerpt_length', 55);
	$excerpt_more = apply_filters('excerpt_more', ' ' . '[...]');
	$excerpt = wp_trim_words( $excerpt, $excerpt_length, $excerpt_more );
	return apply_filters('wp_trim_excerpt', preg_replace($regex,'', $excerpt), $raw_excerpt);
}
add_filter( 'get_the_excerpt', 'wp_strip_header_tags', 9);

// Don't allow TIF's to be uploaded since Chrome doesn't display them.
function unset_tiff($mime_types){
    unset($mime_types['tif|tiff']); //Removing the tif extension
    return $mime_types;
}
add_filter('upload_mimes', 'unset_tiff', 1, 1);

// Register border custom field
if(function_exists("register_field_group"))
{
	register_field_group(array (
		'id' => 'acf_border',
		'title' => 'Border',
		'fields' => array (
			array (
				'key' => 'field_54cb985e55773',
				'label' => 'Border',
				'name' => 'border',
				'type' => 'true_false',
				'message' => '',
				'default_value' => 1,
			),
		),
		'location' => array (
			array (
				array (
					'param' => 'ef_media',
					'operator' => '==',
					'value' => 'all',
					'order_no' => 0,
					'group_no' => 0,
				),
			),
		),
		'options' => array (
			'position' => 'normal',
			'layout' => 'no_box',
			'hide_on_screen' => array (
			),
		),
		'menu_order' => 0,
	));
}

// Generate image that is resized using WP Thumb and that is output using Picturefill http://scottjehl.github.io/picturefill/
function drum_image($id,$small,$medium,$large) {
	$image_alt = get_post_meta($id, '_wp_attachment_image_alt', true);
	$image_url = wp_get_attachment_url( $id );
	$image_small = wpthumb( $image_url, $small['width'] , $small['height'], true, true, true);
	if ($image_small == null) { $image_small = $image_url; }
	$image_medium = wpthumb( $image_url, $medium['width'] , $medium['height'], true, true, true);
	if ($image_medium == null) { $image_medium = $image_url; }
	$image_large = wpthumb( $image_url, $large['width'] , $large['height'], true, true, true);
	if ($image_large == null) { $image_large = $image_url; }
	$image = '<picture itemprop="image">
		<!--[if IE 9]><video style="display: none;"><![endif]-->
		<source srcset="' . $image_large .'" media="(min-width: 64.063em)">
		<source srcset="' . $image_medium . '" media="(min-width: 40.063em)">
		<!--[if IE 9]></video><![endif]-->

		<!--[if lt IE 9]>
		<img src="' . $image_large .'" alt="' . $image_alt . '">
		<![endif]-->

		<!--[if !lt IE 9]><!-->
		<img srcset="' . $image_small .'" alt="' . $image_alt . '">
		<![endif]-->
	</picture>';
	return $image;
}

// Add no-border class to gallery items
function add_class_to_gallery($link, $id) {
	$border = true;
	$border = get_field('border', $id);
	if (!$border) {
		return str_replace('attachment-thumbnail', 'attachment-thumbnail no-border', $link);
	} else {
		return $link;
	}
}
add_filter( 'wp_get_attachment_link', 'add_class_to_gallery', 10, 2 );

// Add no-border class to images
function add_class_to_image($html, $id, $alt, $title, $align, $url, $size) {
	$border = true;
	$border = get_field('border', $id);
	if (!$border) {
		return str_replace('class="', 'class="no-border ', $html);
	} else {
		return $html;
	}
}
add_filter( 'image_send_to_editor', 'add_class_to_image', 10, 8);

// Add stylesheet to WYSIWYG
function drum_beat_add_editor_styles() {
    add_editor_style( 'css/wysiwyg-style.css' );
}
add_action( 'admin_init', 'drum_beat_add_editor_styles' );

add_action( 'tgmpa_register', 'my_theme_register_required_plugins' );
/**
 * Register the required plugins for this theme.
 */
function my_theme_register_required_plugins() {
    $plugins = array(

        // Require Gravity Forms
        array(
            'name'               => 'Gravity Forms', // The plugin name.
            'slug'               => 'gravityforms', // The plugin slug (typically the folder name).
            'source'             => 'http://drumcreative.com/wp-content/required-plugins/gravityforms.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),

        // ACF Pro
        array(
            'name'               => 'Advanced Custom Fields Pro', // The plugin name.
            'slug'               => 'advanced-custom-fields-pro', // The plugin slug (typically the folder name).
            'source'             => 'http://drumcreative.com/wp-content/required-plugins/advanced-custom-fields-pro.zip', // The plugin source.
            'required'           => true, // If false, the plugin is only 'recommended' instead of required.
        ),

        // Require Live Edit
        array(
            'name'      => 'Live Edit',
            'slug'      => 'live-edit',
            'required'  => true,
        ),

        // Require Wordpress SEO
        array(
            'name'      => 'WordPress SEO by Yoast',
            'slug'      => 'wordpress-seo',
            'required'  => true,
        ),

        // Require WP Thumb
        array(
            'name'      => 'WP Thumb',
            'slug'      => 'wp-thumb',
            'required'  => true,
        ),

    );
    $config = array(
        'default_path' => '',                      // Default absolute path to pre-packaged plugins.
        'menu'         => 'tgmpa-install-plugins', // Menu slug.
        'has_notices'  => true,                    // Show admin notices or not.
        'dismissable'  => true,                    // If false, a user cannot dismiss the nag message.
        'dismiss_msg'  => '',                      // If 'dismissable' is false, this message will be output at top of nag.
        'is_automatic' => false,                   // Automatically activate plugins after installation or not.
        'message'      => '',                      // Message to output right before the plugins table.
        'strings'      => array(
            'page_title'                      => __( 'Install Required Plugins', 'tgmpa' ),
            'menu_title'                      => __( 'Install Plugins', 'tgmpa' ),
            'installing'                      => __( 'Installing Plugin: %s', 'tgmpa' ), // %s = plugin name.
            'oops'                            => __( 'Something went wrong with the plugin API.', 'tgmpa' ),
            'notice_can_install_required'     => _n_noop( 'This theme requires the following plugin: %1$s.', 'This theme requires the following plugins: %1$s.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_can_install_recommended'  => _n_noop( 'This theme recommends the following plugin: %1$s.', 'This theme recommends the following plugins: %1$s.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_cannot_install'           => _n_noop( 'Sorry, but you do not have the correct permissions to install the %s plugin. Contact the administrator of this site for help on getting the plugin installed.', 'Sorry, but you do not have the correct permissions to install the %s plugins. Contact the administrator of this site for help on getting the plugins installed.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_can_activate_required'    => _n_noop( 'The following required plugin is currently inactive: %1$s.', 'The following required plugins are currently inactive: %1$s.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_can_activate_recommended' => _n_noop( 'The following recommended plugin is currently inactive: %1$s.', 'The following recommended plugins are currently inactive: %1$s.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_cannot_activate'          => _n_noop( 'Sorry, but you do not have the correct permissions to activate the %s plugin. Contact the administrator of this site for help on getting the plugin activated.', 'Sorry, but you do not have the correct permissions to activate the %s plugins. Contact the administrator of this site for help on getting the plugins activated.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_ask_to_update'            => _n_noop( 'The following plugin needs to be updated to its latest version to ensure maximum compatibility with this theme: %1$s.', 'The following plugins need to be updated to their latest version to ensure maximum compatibility with this theme: %1$s.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'notice_cannot_update'            => _n_noop( 'Sorry, but you do not have the correct permissions to update the %s plugin. Contact the administrator of this site for help on getting the plugin updated.', 'Sorry, but you do not have the correct permissions to update the %s plugins. Contact the administrator of this site for help on getting the plugins updated.', 'drum-beat-6-starter-theme' ), // %1$s = plugin name(s).
            'install_link'                    => _n_noop( 'Begin installing plugin', 'Begin installing plugins', 'drum-beat-6-starter-theme' ),
            'activate_link'                   => _n_noop( 'Begin activating plugin', 'Begin activating plugins', 'drum-beat-6-starter-theme' ),
            'return'                          => __( 'Return to Required Plugins Installer', 'tgmpa' ),
            'plugin_activated'                => __( 'Plugin activated successfully.', 'tgmpa' ),
            'complete'                        => __( 'All plugins installed and activated successfully. %s', 'tgmpa' ), // %s = dashboard link.
            'nag_type'                        => 'updated' // Determines admin notice type - can only be 'updated', 'update-nag' or 'error'.
        )
    );
    tgmpa( $plugins, $config );
}

// Add Schema.org support
function html_tag_schema() {
    $schema = 'http://schema.org/';
    // Is single post
    if(is_single()) {
        $type = "Article";
    }
    // Is author page
    elseif( is_author() ) {
        $type = 'ProfilePage';
    }
    // Contact form page ID
    elseif( is_page(1) )
    {
        $type = 'ContactPage';
    }
    // Is search results page
    elseif( is_search() ) {
        $type = 'SearchResultsPage';
    }
    else {
        $type = 'WebPage';
    }
    echo 'itemscope="itemscope" itemtype="' . $schema . $type . '"';
}


// Enable automatic plugin updates
add_filter( 'auto_update_plugin', '__return_true' );

// Hide admin bar for Subscribers
add_action('set_current_user', 'cc_hide_admin_bar');
function cc_hide_admin_bar() {
  if (!current_user_can('edit_posts')) {
    show_admin_bar(false);
  }
}

// Disable comments on media attachments
function filter_media_comment_status( $open, $post_id ) {
	$post = get_post( $post_id );
	if( $post->post_type == 'attachment' ) {
		return false;
	}
	return $open;
}
add_filter( 'comments_open', 'filter_media_comment_status', 10 , 2 );

// Add User Browser and OS Classes to Body Class
function mv_browser_body_class($classes) {
        global $is_lynx, $is_gecko, $is_IE, $is_opera, $is_NS4, $is_safari, $is_chrome, $is_iphone;
        if($is_lynx) $classes[] = 'lynx';
        elseif($is_gecko) $classes[] = 'gecko';
        elseif($is_opera) $classes[] = 'opera';
        elseif($is_NS4) $classes[] = 'ns4';
        elseif($is_safari) $classes[] = 'safari';
        elseif($is_chrome) $classes[] = 'chrome';
        elseif($is_IE) {
                $classes[] = 'ie';
                if(preg_match('/MSIE ([0-9]+)([a-zA-Z0-9.]+)/', $_SERVER['HTTP_USER_AGENT'], $browser_version))
                $classes[] = 'ie'.$browser_version[1];
        } else $classes[] = 'unknown';
        if($is_iphone) $classes[] = 'iphone';
        if ( stristr( $_SERVER['HTTP_USER_AGENT'],"mac") ) {
                 $classes[] = 'osx';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"linux") ) {
                 $classes[] = 'linux';
           } elseif ( stristr( $_SERVER['HTTP_USER_AGENT'],"windows") ) {
                 $classes[] = 'windows';
           }
        return $classes;
}
add_filter('body_class','mv_browser_body_class');

// Remove "Custom Fields" meta box to speed up admin
function admin_speedup_remove_post_meta_box() {
	global $post_type;
	if ( is_admin()  && post_type_supports( $post_type, 'custom-fields' )) {
		remove_meta_box( 'postcustom', $post_type, 'normal' );
	}
}
add_action( 'add_meta_boxes', 'admin_speedup_remove_post_meta_box' );

// Add field type class to Gravity Forms fields
add_filter( 'gform_field_css_class', 'gf_field_type_classes', 10, 3 );
function gf_field_type_classes( $classes, $field, $form ) {
    $classes .= ' gfield_' . $field->type;
    return $classes;
}
?>